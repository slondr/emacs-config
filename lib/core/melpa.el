;;; melpa.el
;;; Code for enabling and configuring MELPA

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t) ; melpa
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t) ; gnu elpa

;; The following lines are required for emacs init files to reference packages
(setq package-enable-at-startup nil)
(package-initialize)

;; Enable use-package
(require 'use-package)
(require 'use-package-ensure)
(setq use-package-always-ensure t)

;; this makes for an always-loadable user prefix
(global-unset-key (kbd "M-SPC"))

;; Global keybinding to launch package list
(global-set-key (kbd "M-SPC p") #'package-list-packages)
