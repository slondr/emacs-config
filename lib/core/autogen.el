;;; autogen.el
;;; This file contains all automatically generated code 

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(connection-local-criteria-alist
	 '(((:application eshell)
			eshell-connection-default-profile)
		 ((:application tramp)
			tramp-connection-local-default-system-profile tramp-connection-local-default-shell-profile)))
 '(connection-local-profile-alist
	 '((eshell-connection-default-profile
			(eshell-path-env-list))
		 (tramp-connection-local-darwin-ps-profile
			(tramp-process-attributes-ps-args "-acxww" "-o" "pid,uid,user,gid,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" "-o" "state=abcde" "-o" "ppid,pgid,sess,tty,tpgid,minflt,majflt,time,pri,nice,vsz,rss,etime,pcpu,pmem,args")
			(tramp-process-attributes-ps-format
			 (pid . number)
			 (euid . number)
			 (user . string)
			 (egid . number)
			 (comm . 52)
			 (state . 5)
			 (ppid . number)
			 (pgrp . number)
			 (sess . number)
			 (ttname . string)
			 (tpgid . number)
			 (minflt . number)
			 (majflt . number)
			 (time . tramp-ps-time)
			 (pri . number)
			 (nice . number)
			 (vsize . number)
			 (rss . number)
			 (etime . tramp-ps-time)
			 (pcpu . number)
			 (pmem . number)
			 (args)))
		 (tramp-connection-local-busybox-ps-profile
			(tramp-process-attributes-ps-args "-o" "pid,user,group,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" "-o" "stat=abcde" "-o" "ppid,pgid,tty,time,nice,etime,args")
			(tramp-process-attributes-ps-format
			 (pid . number)
			 (user . string)
			 (group . string)
			 (comm . 52)
			 (state . 5)
			 (ppid . number)
			 (pgrp . number)
			 (ttname . string)
			 (time . tramp-ps-time)
			 (nice . number)
			 (etime . tramp-ps-time)
			 (args)))
		 (tramp-connection-local-bsd-ps-profile
			(tramp-process-attributes-ps-args "-acxww" "-o" "pid,euid,user,egid,egroup,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" "-o" "state,ppid,pgid,sid,tty,tpgid,minflt,majflt,time,pri,nice,vsz,rss,etimes,pcpu,pmem,args")
			(tramp-process-attributes-ps-format
			 (pid . number)
			 (euid . number)
			 (user . string)
			 (egid . number)
			 (group . string)
			 (comm . 52)
			 (state . string)
			 (ppid . number)
			 (pgrp . number)
			 (sess . number)
			 (ttname . string)
			 (tpgid . number)
			 (minflt . number)
			 (majflt . number)
			 (time . tramp-ps-time)
			 (pri . number)
			 (nice . number)
			 (vsize . number)
			 (rss . number)
			 (etime . number)
			 (pcpu . number)
			 (pmem . number)
			 (args)))
		 (tramp-connection-local-default-shell-profile
			(shell-file-name . "/bin/sh")
			(shell-command-switch . "-c"))
		 (tramp-connection-local-default-system-profile
			(path-separator . ":")
			(null-device . "/dev/null"))))
 '(custom-safe-themes
	 '("a5270d86fac30303c5910be7403467662d7601b821af2ff0c4eb181153ebfc0a" "467dc6fdebcf92f4d3e2a2016145ba15841987c71fbe675dcfe34ac47ffb9195" "c4063322b5011829f7fdd7509979b5823e8eea2abf1fe5572ec4b7af1dd78519" "be9645aaa8c11f76a10bcf36aaf83f54f4587ced1b9b679b55639c87404e2499" "7f1d414afda803f3244c6fb4c2c64bea44dac040ed3731ec9d75275b9e831fe5" "830877f4aab227556548dc0a28bf395d0abe0e3a0ab95455731c9ea5ab5fe4e1" "2809bcb77ad21312897b541134981282dc455ccd7c14d74cc333b6e549b824f3" "b89ae2d35d2e18e4286c8be8aaecb41022c1a306070f64a66fd114310ade88aa" "70dc10dd01a3f69382999a7e1484106fda480c927907776853d374dc43ff0f5e" "f9aede508e587fe21bcfc0a85e1ec7d27312d9587e686a6f5afdbb0d220eab50" default))
 '(electric-pair-mode t)
 '(erc-modules
	 '(autojoin button completion fill irccontrols list match menu move-to-prompt netsplit networks noncommands notifications readonly ring stamp spelling track))
 '(erc-nick "slondr")
 '(eshell-modules-list
	 '(eshell-alias eshell-banner eshell-basic eshell-cmpl eshell-dirs eshell-ls eshell-xtra eshell-extpipe eshell-glob eshell-hist eshell-ls eshell-pred eshell-prompt eshell-script eshell-term eshell-unix))
 '(org-babel-load-languages '((lisp . t) (emacs-lisp . t) (R . t)))
 '(org-latex-default-packages-alist
	 '(("AUTO" "inputenc" t
			("pdflatex"))
		 ("T1" "fontenc" t
			("pdflatex"))
		 ("" "graphicx" t nil)
		 ("" "grffile" t nil)
		 ("" "longtable" nil nil)
		 ("" "wrapfig" nil nil)
		 ("" "rotating" nil nil)
		 ("normalem" "ulem" t nil)
		 ("" "amsmath" t nil)
		 ("" "textcomp" t nil)
		 ("" "amssymb" t nil)
		 ("" "capt-of" nil nil)
		 ("colorlinks=true" "hyperref" t nil)))
 '(org-latex-packages-alist '(("" "minted")) nil nil "Customized with use-package org")
 '(org-modules
	 '(ol-bbdb ol-bibtex ol-docview ol-eww ol-gnus ol-info ol-irc ol-mhe ol-rmail org-tempo ol-w3m))
 '(org-time-stamp-custom-formats '(" %A, %B %e, %Y " . "<%m/%d/%y %a %H:%M>"))
 '(package-selected-packages
	 '(lsp treemacs-projectile gleam-ts-mode all-the-icons treesit-auto plantuml-mode terraform-mode dirvish ledger-mode nix-mode gpr-yasnippets gpr-mode ada-mode exec-path-from-shell orderless yasnippet-snippets treemacs opam eglot embark marginalia vertico elixir-yasnippets elixir-yasnippest flymake-easy flymake-elixir "fill-column-indicator" "fci-mode" fill-column-indicator smart-tabs-mode elixir-ts-mode heex-ts-mode typst-mode eat ripgrep zig-mode forge tree-sitter salt-mode graphviz-dot-mode company-qml qml-mode just-mode hcl-mode fuel apparmor-mode lsp-pyright yasnippet nhexl-mode arduino-mode stumpwm-mode raku-mode haskell-mode org-roam-ui caddyfile-mode org gemini-mode ox-gemini org-contrib 0blayout ement plz quelpa-use-package ron-mode nginx-mode sdlang-mode graphql-mode w3m qt-pro-mode package-lint csharp-mode geiser lua-mode auctex rustic meson-mode alchemist flycheck-elixir org-noter company-erlang lorem-ipsum ox-reveal org-reveal org-sidebar sly company-org-roam flycheck-grammarly flymake-grammerly lfe-mode dashboard org-roam-protocol ess elpher org-download ox-latex org-bullets speed-type lsp-java elfeed-web erlang groovy-mode mw-thesaurus telega org-journal erc-hl-nicks org-msg org-mu4e use-package-ensure-system-package use-package-chords ox-hugo define-word gnuplot hydra docker-compose-mode dockerfile-mode latex-preview-pane org-roam-server fish-mode symon doom-themes org-roam use-package elfeed-org emojify todoist rjsx-mode 2048-game elfeed flx-ido projectile typescript-mode diminish doom-modeline bison-mode go-mode web-narrow-mode pdf-tools solarized-theme gruvbox-theme sass-mode org-brain cmake-mode svelte-mode gitlab-ci-mode-flycheck vterm web-mode handlebars-mode company-lsp lsp-ui lsp-mode toml-mode cargo flycheck-rust rust-mode company-tern writeroom-mode wc-mode company-shell company-web js2-mode utop flycheck-ocaml flycheck-color-mode-line tuareg merlin pkgbuild-mode rainbow-delimiters magit monokai-theme))
 '(safe-local-variable-values
	 '((TeX-engine . xelatex)
		 (org-html-postamble . t)
		 (org-confirm-babel-evaluate)
		 (eval setq-local org-roam-directory default-directory)))
 '(send-mail-function 'smtpmail-send-it)
 '(utop-command "opam config exec -- utop -emacs")
 '(vterm-kill-buffer-on-exit t)
 '(web-mode-auto-close-style 1)
 '(web-mode-auto-quote-style 1)
 '(web-mode-markup-indent-offset 2))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
