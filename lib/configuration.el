;;; backups.el --- Configuration for emacs backups

(setq backup-by-copying t			; Don't clobber symlinks in backups
      backup-directory-alist '(("." . "~/.emacs.d/saves/")) ; Don't litter my fs tree with backups
      kept-new-versions 6				       ; for backups
      kept-old-versions 2				       ; for backups
      delete-old-versions t				       ; for backups
      version-control t)				       ; Use versioned backups
;;; dashboard.el --- A fun splash screen

(use-package dashboard
  :custom (initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
  :config (dashboard-setup-startup-hook))
;;; diminish.el --- Diminishing minor modes I don't care about
;;; Code:

(require 'diminish)
(eval-after-load "company" '(diminish 'company-mode))
(eval-after-load "flycheck" '(diminish 'flycheck-mode))
(eval-after-load 'auto-revert-mode (diminish 'auto-revert-mode))
(eval-after-load "projectile" '(diminish 'projectile-mode))
;; -*- lexical-binding: t -*-
;;; elfeed.el --- Configuration for elfeed
(require 'elfeed-org)
(elfeed-org)
(setq rmh-elfeed-org-files (list "~/.emacs.d/lib/elfeed.org"))

(global-set-key (kbd "C-c r") 'elfeed)	; Global keybinding for elfeed

;; Programming
(use-package yasnippet-snippets)

(use-package lsp-mode
	:init (add-to-list 'exec-path "~/Packages/elixir-ls/release"))

;; Tree-sitter configuration needs to come before some modes that require it
(use-package tree-sitter)

(add-to-list 'load-path "~/Packages/tree-sitter-indent.el/")
(require 'tree-sitter-indent)


;; elfeed "split-pane" view
(setq elfeed-show-entry-switch #'elfeed-display-buffer)

(defun elfeed-display-buffer (buf &optional act)
  (pop-to-buffer buf)
  (set-window-text-height (get-buffer-window) (round (* 0.7 (frame-height)))))

(defun elfeed-search-show-entry-pre (&optional lines)
  "Returns a function to scroll forward or back in the Elfeed
  search results, displaying entries without switching to them."
  (lambda (times)
    (interactive "p")
    (forward-line (* times (or lines 0)))
    (recenter)
    (call-interactively #'elfeed-search-show-entry)
    (select-window (previous-window))
    (unless elfeed-search-remain-on-entry (forward-line -1))))

(define-key elfeed-search-mode-map (kbd "n") (elfeed-search-show-entry-pre +1))
(define-key elfeed-search-mode-map (kbd "p") (elfeed-search-show-entry-pre -1))
(define-key elfeed-search-mode-map (kbd "M-RET") (elfeed-search-show-entry-pre))
;;; elixir.el --- configuration for the elixir programming language

(use-package elixir-ts-mode)

(use-package flycheck-elixir)

(use-package flymake-easy)
(use-package flymake-elixir)
;(use-package alchemist)

(use-package elixir-yasnippets)

;;; elpher.el --- Configuration for a gopher and gemini client

(use-package elpher
  :custom
  (elpher-gemini-max-fill-width 132))
;;; email.el --- Configuration for using Emacs as an email client

(use-package mu4e
  :load-path "/usr/share/emacs/site-lisp/mu4e"
  :ensure nil
  :ensure-system-package mu
  :custom
  (mu4e-attachment-dir "~/Downloads/Email")
  (mu4e-compose-signature-auto-include nil)
  (mu4e-drafts-folder "/Drafts")
  (mu4e-get-mail-command "mbsync -a")
  (mu4e-maildir "/Documents/Email/Maildir")
  (mu4e-refile-folder "/Archive")
  (mu4e-sent-folder "/Sent")
  (mu4e-trash-folder "/Trash")
  (mu4e-update-interval 300)
  (mu4e-use-fancy-chars t)
  (mu4e-view-show-addresses t)
  (mu4e-view-show-images t)
					;(mu4e-html2text-command "iconv -c -t utf-8 | pandoc -f html -t plain") ; use pandoc for html email
  (mu4e-compose-format-flowed t)
  (message-kill-buffer-on-exit t)
  (user-mail-address "ericlondres@outlook.com")
  (user-full-name "Eric S. Londres")
  (smtpmail-smtp-server "smtp.office365.com") ; use office365 for SMTP outgoing mail
  (smtpmail-smtp-service 587)		      ; use port 587 for SMTP (required by office365)
  (smtpmail-stream-type nil)	      ; use STARTTLS for SMTP (required by office365)
  :config
					; (add-to-list 'mu4e-view-actions '("ViewInBrowser" . mu4e-action-view-in-browser) t) ; superseded by mu4e itself
  (global-set-key (kbd "M-SPC e") 'mu4e)
  (add-hook 'mu4e-compose-mode-hook (lambda () (use-hard-newlines -1))))

;; org-msg support
(use-package org-msg
  :after (mu4e)
  :init
  (setq mail-user-agent 'mu4e-user-agent)
  :custom
  (org-msg-options "html-postamble:nil num:nil toc:nil author:nil")
  (org-msg-default-alternatives '(html text))
  :config
  (org-msg-mode))

;;; erc.el --- Customization for the Emacs IRC client
;;; Commentary: Configuration of Emacs Relay Chat
;;; Code:

;; Disable global rainbow-delimiters-mode when in an ERC buffer (it looks ugly)
(add-hook 'erc-mode-hook (lambda () (rainbow-delimiters-mode -1)))

(setq erc-hide-list '("JOIN" "PART" "QUIT") ; Don't show join/part/quit messages
      erc-fill-column 100		    ; ERC full buffer width to 100 characters
      erc-fill-function 'erc-fill-static    ; Statically set width of all elements
      erc-fill-static-center 20)	    ; Center (divider between nicks and messages) to column 20

;; Highlight nicknames to make them visually distinct
(use-package erc-hl-nicks
  :after
  (erc-hl-nicks-enable))

(make-variable-buffer-local 'erc-fill-static-center)

(defun get-nicks (channel-users)
  (hash-table-keys channel-users))

(defun get-longest-nick-length (channel-users)
  "Return the longest nickname "
  (car (reverse (sort (mapcar 'length (get-nicks channel-users)) #'<))))

(defun erc-right-align-nicks (_)
  (dolist (b (buffer-list))
    (with-current-buffer b
      (when erc-channel-users
	(let ((right-margin (+ 2 (get-longest-nick-length erc-channel-users))))
	  (setq erc-fill-static-center right-margin))))))

(add-hook 'erc-timer-hook 'erc-right-align-nicks)
;;; etc.el --- For initialization code that doesn't realy fit anywhere else.
;;; Commentary:  Initial set-up code for look and feel
;;; Code:

(scroll-bar-mode -1)			; Disable scroll bars visually
(tool-bar-mode -1)                      ; Disable icon tool bar
(menu-bar-mode -1)			; Disble the top menubar

;; Default font
(set-face-attribute 'default nil :family "monospace" :height 90 :width 'normal)
(copy-face 'default 'fixed-pitch)

;; Configuration for backups
(setq
 inhibit-startup-streen t 		; Disable splash screen
 initial-scratch-message nil		; Clear *scratch*'s content on init
 browse-url-browser-function 'browse-url-generic
 browse-url-generic-program "firefox-developer-edition") ; set default browser

(use-package all-the-icons)		; Icons are fun

(use-package pdf-tools :config (pdf-tools-install)) ; A better PDF viewer

(use-package lisp-extra-font-lock
  :config
  (lisp-extra-font-lock-global-mode 1))

;; Fast multi-file content search
(use-package ripgrep)

;; Programming language support
(use-package lfe-mode)

;; Terminal emulation support
(use-package eat)

;; Eshell stuff
;; enables terminal emulation in eshell
(add-hook 'eshell-load-hook #'eat-eshell-visual-command-mode) 

;;  Environment variables for eshell
(setenv "VISUAL" "emacsclient")
(setenv "EDITOR" "emacsclient")

;; Programming
(add-hook 'prog-mode-hook #'(lambda () (yas-minor-mode t)))
(add-hook 'prog-mode-hook #'(lambda () (display-line-numbers-mode t)))

;; Document typesetting
(use-package polymode)
(use-package typst-mode)

;; Assumes web-mode and elixir-mode are already set up
(use-package polymode
  :mode ("\.ex$" . poly-elixir-web-mode)
  :config
  (define-hostmode poly-elixir-hostmode :mode 'elixir-mode)
  (define-innermode poly-liveview-expr-elixir-innermode
    :mode 'web-mode
    :head-matcher (rx line-start (* space) "~H" (= 3 (char "\"'")) line-end)
    :tail-matcher (rx line-start (* space) (= 3 (char "\"'")) line-end)
    :head-mode 'host
    :tail-mode 'host
    :allow-nested nil
    :keep-in-mode 'host
    :fallback-mode 'host)
  (define-polymode poly-elixir-web-mode
    :hostmode 'poly-elixir-hostmode
    :innermodes '(poly-liveview-expr-elixir-innermode))
  )

(setq web-mode-engines-alist '(("elixir" . "\\.ex\\'"))) 

;; Use tabs for indentation
(setq-default tab-width 2)
(defvaralias 'c-basic-offset 'tab-width)
(defvaralias 'cperl-indent-level 'tab-width)

;; Completion!
(use-package company)
(use-package vertico :init (vertico-mode))
(use-package marginalia
  :bind (("M-A" . marginalia-cycle)
	 :map minibuffer-local-map ("M-A" . marginalia-cycle))
  :init (marginalia-mode))

;; use the `orderless' completion style.
(use-package orderless
  :init
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-consult-dispatch orderless-affix-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))

(use-package embark
  :bind (("C-," . embark-act)
	 ("C-;" . embark-dwim)
	 ("C-h B" . embark-bindings))
  :custom (prefix-help-command #'embark-prefix-help-command)
  :config (add-to-list 'display-buffer-alist
		       '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*'"
			 nil
			 (window-parameters (mode-line-format . none)))))

;;; Code Ends Here
;;; fc.el
;;; Configuration for Fill Column Indicator

(setq-default fill-column 88)
(global-display-fill-column-indicator-mode t)
;;; fn.el
;;; Custom functions

;; Closes all active buffers
(defun kill-all-buffers () (interactive) (mapc 'kill-buffer (buffer-list)))

;; Safely kill the Emacs server
(defun shutdown ()
  "Save buffers and kill the server"
  (interactive)
  (save-some-buffers)
  (kill-emacs))
(use-package magit)

(use-package forge :after magit :custom (auth-sources '("~/.authinfo.gpg")))

;; Gleam is a programming language with an official emacs mode that isn't in Melpa.
(use-package gleam-ts-mode)

;;; global.el
;;; Code which enables modes globally

;; Enables rainbow-delins in all buffers
(define-globalized-minor-mode global-rainbow-mode rainbow-delimiters-mode
  (lambda () (rainbow-delimiters-mode 1)))
(global-rainbow-mode 1)

;; Enables company-mode everywhere
(add-hook 'after-init-hook 'global-company-mode)

;; Enable column-numbers everywhere
(add-hook 'after-init-hook #'column-number-mode)
(use-package graphviz-dot-mode)

(require 'company-tern)			
(add-to-list 'company-backends 'company-tern) ; Use Tern as a backend for company-mode
(add-hook 'js2-mode-hook (lambda () (tern-mode)))
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))	; Set js2-mode as the mode for .js* files
(add-hook 'js2-mode-hook #'js2-imenu-extras-mode) ; better imenu with js2
(add-hook 'js2-mode-hook #'lsp)
;;; key.el
;;; Custom keybindings

(global-set-key (kbd "C-x g") #'magit-status) ; C-x g spawns magit window
(global-set-key (kbd "<f5>") #'compile)	; F5 runs `compile`
(global-set-key (kbd "<Calculator>") #'full-calc) ; Open a full calculator when pressing calculator key
(global-set-key (kbd "C-x C-d") #'define-word-at-point) ; Show the dictionary definition of the word at the point
(global-unset-key (kbd "C-z"))		; Removes dumbass keybinding shits everything
;;; stuff for interacting with matrix

;;; modeline.el --- For customization of the modeline

(use-package doom-modeline
  :custom
  (doom-modeline-minor-modes t)
  (doom-modeline-enable-word-count t)
  (doom-modeline-continuous-word-count-modes '(org-mode))
  :config
  (doom-modeline-mode 1)
  (add-hook 'server-after-make-frame-hook
	    (lambda () (setq doom-modeline-icon (display-graphic-p)))))
;;; ocaml.el
;;; Configuration for OCaml programming

(setq auto-mode-alist
      (append '(("\\.ml[ily]?$" . tuareg-mode) ; use tuareg for ocaml files
                ("\\.topml$" . tuareg-mode)) ; use tuareg for topml files
              auto-mode-alist)) 
(autoload 'utop-minor-mode "utop" "Toplevel for OCaml" t)  ; Load the utop minor mode
(add-hook 'tuareg-mode-hook 'utop-minor-mode) ; Setup utop when entering tuareg mode
(add-hook 'tuareg-mode-hook 'merlin-mode) ; Enable merlin when entering tuareg mode
(setq merlin-use-auto-complete-mode t) 	; ac-mode for Merlin
(setq merlin-error-after-save nil)
;;; org.el --- Customization related to org-mode

;; Set the default LaTeX engine to XeLaTeX
(setq org-latex-pdf-process
      '("xelatex -8bit --enable-8bit-chars -shell-escape -interaction nonstopmode -output-directory %o %f" ; First pass
	"xelatex -8bit --enable-8bit-chars -shell-escape -interaction nonstopmode -output-directory %o %f"))	; Multiple passes

(require 'ox-publish)

(defun create-blog-post ()
  (interactive)
  (let ((name (read-string "Filename: ")))
    (expand-file-name (format "%s.org" name) "~/org/blog/posts/")))

(defun blog-deploy ()
  (interactive)
  (let ((wd default-directory))
    (org-publish-all)
    (cd "~/org/blog/")
    (load-file "deploy.el")
    (cd wd)))

(defun my-sitemap (title list)
  "Custom sitemap generator with additional options"
  (concat "\n#+setupfile: /home/eric/org/blog/org-template/style.org\n" (org-publish-sitemap-default title list)))

(use-package org
  :pin gnu
  :custom
  (org-directory "~/Documents/Notes")
  (org-latex-listings 'minted)
  (org-latex-minted-options '(("frame" "lines") ("linenos=true")))
  (org-latex-packages-alist '(("" "minted")))
  (org-html-doctype "html5")		; default to HTML5 output
  (org-html-html5-fancy t)		; enable fancy new tags
  (org-html-postamble nil)		; disable the postamble
  (org-html-postamble-format '(("en" "<hr></hr><p class=\"author\">Author: %a</p><p class=\"date\">Date: %d</p>")))
  (org-html-preamble nil)		; disable the preamble
  (org-html-metadata-timestamp-format "%u, %B %e, %Y")
  (org-publish-project-alist '(("posts"
				:base-directory "~/org/blog/posts/"
				:base-extension "org"
				:publishing-directory "~/org/blog/public/"
				:recursive t
				:publishing-function org-html-publish-to-html
				:auto-sitemap t
				:sitemap-title "Eric S. Londres's Blorg"
				:sitemap-filename "index.org"
				:sitemap-style tree
				:sitemap-sort-files anti-chronologically
				:sitemap-function my-sitemap
				:html-link-home "/"
				:html-link-up "../"
				:author "Eric S. Londres"
				:email "EricLondres@outlook.com"
				:with-creator t)
			       ("css"
				:base-directory "~/org/blog/css/"
				:base-extension "css"
				:publishing-directory "~/org/blog/public/css"
				:publishing-function org-publish-attachment
				:recursive t)
			       ("all" :components ("posts" "css"))))
  (org-preview-latex-process-alist
   (quote
    ((dvipng :programs ("lualatex" "dvipng")
             :description "dvi > png" :message "you need to install the programs: latex and dvipng." :image-input-type "dvi" :image-output-type "png" :image-size-adjust
             (1.0 . 1.0)
             :latex-compiler
             ("lualatex -output-format dvi -interaction nonstopmode -output-directory %o %f")
             :image-converter
             ("dvipng -fg %F -bg %B -D %D -T tight -o %O %f"))
     (dvisvgm :programs ("latex" "dvisvgm")
              :description "dvi > svg" :message "you need to install the programs: latex and dvisvgm." :use-xcolor t :image-input-type "xdv" :image-output-type "svg" :image-size-adjust
              (1.7 . 1.5) :latex-compiler
              ("xelatex -no-pdf -interaction nonstopmode -output-directory %o %f")
              :image-converter ("dvisvgm %f -n -b min -c %S -o %O"))
     (imagemagick :programs
		  ("latex" "convert")
		  :description "pdf > png" :message "you need to install the programs: latex and imagemagick." :use-xcolor t :image-input-type "pdf" :image-output-type "png" :image-size-adjust
		  (1.0 . 1.0)
		  :latex-compiler
		  ("xelatex -no-pdf -interaction nonstopmode -output-directory %o %f")
		  :image-converter
		  ("convert -density %D -trim -antialias %f -quality 100 %O")))))
  (org-preview-latex-default-process 'dvisvgm)
  (org-agenda-files '("~/org/agenda/"))
  :config
  (require 'ox-md) ; enable markdown export
  (add-hook 'org-mode-hook
	    (lambda ()
	      (org-indent-mode)
	      (visual-line-mode)
	      (flyspell-mode)))
  (global-set-key (kbd "C-c n c") 'org-capture)
  (setq org-capture-templates '(("p" "Post" plain
				 (file create-blog-post)
				 (file "~/.emacs.d/org-templates/post.orgcaptmpl"))
				("j" "entry" entry (file+datetree "~/org-roam/journal.org.gpg") "* %?" :empty-lines 1)))
  (require 'ox-latex)
  (with-eval-after-load 'ox-latex
    (add-to-list 'org-latex-classes '("letter" "\\documentclass{letter}" ("\\begin{letter}"
									  "\\end{letter}"
									  "\\begin{letter}"
									  "\\end{letter}")))))

(use-package org-bullets :after (org) :config (add-hook 'org-mode-hook (lambda () (org-bullets-mode))))

(use-package org-download :after (org) :config (add-hook 'dired-mode-hook 'org-download-enable))

;; org-reveal
(use-package ox-reveal :after (org)
  :custom (org-reveal-root "https://cdn.jsdelivr.net/npm/reveal.js"))

;; notation mode
(use-package org-noter :after (org))

;; extra fun stuff
(use-package org-contrib :after (org))

;; gemini
(use-package ox-gemini :after (org))
;;; projectile.el --- Initialization and configuration for Projectile 

(use-package projectile
  :bind-keymap ("C-c p" . projectile-command-map)
	:custom (projectile-project-search-path '(("~/Development" . 2)))
	:config (projectile-discover-projects-in-search-path))
;;; python.el --- configuration for python programming

(use-package lsp-python-ms
  :custom (lsp-python-ms-auto-update-server t)
  :hook (python-mode . (lambda () (require 'lsp-python-ms) (lsp))))
;;; r.el --- Configuration for using the R programming language

(use-package ess)
;;; roam.el --- Enabling and configuring org-roam
;;; Code:

;; Workaround for new GnuPG versions being incompatible with Emacs or something like that
(fset 'epg-wait-for-status 'ignore)
(setq-default epa-file-encrypt-to '("ericlondres@protonmail.com"))

(use-package org-roam
  :ensure t
  :diminish org-roam-mode
  :hook
  (after-init . org-roam-mode)
  :custom
  (org-roam-directory "/home/eric/org-roam/")
  (org-roam-index-file "20200701012641-index.org")
  (org-roam-graph-exclude-matcher '("index")) ; Don't include the index file in graphing or roam-server
  (org-roam-complete-everywhere t)
	(org-roam-capture-templates '(("d" "default" plain "%?"
																 :target (file+head "${slug}.org.gpg"
																										"#+title: ${title}\n")
																 :unnarrowed t)))
  (org-roam-encrypt-files t)		; Don't encrypt new entries with PGP
  :bind
  (("C-c n l" . org-roam)
   ("C-c n f" . org-roam-node-find)
   :map org-mode-map
   (("C-c n i" . org-roam-node-insert))
   (("C-c n I" . org-roam-insert-immediate))))

;; Configuration for org-journal
(use-package org-journal
  :bind ("C-c n j" . org-journal-new-entry)
  :custom
  (org-journal-date-prefix "#+roam_tags:journal\n#+title: ")
  (org-journal-dir "~/org-roam/")
  (org-journal-date-format "%A, %d %B %Y"))


;; Eglot's type hints are sometimes useful and usually very annoying.
(add-hook 'eglot-managed-mode-hook (lambda () (eglot-inlay-hints-mode -1)))

;; Enable symbol prettification globally
(global-prettify-symbols-mode 1)

;; Fun UI
(use-package org-roam-ui)
;;; rust.el --- Configuration for rust-mode

(use-package rustic
  :custom
  (rustic-lsp-client 'eglot)
  (rustic-lsp-server 'rust-analyzer)
  :config
  (add-hook 'rustic-mode-hook
	    (lambda ()
	      (setq prettify-symbols-alist
		    '(("Fn"  . ?𝘍)
		      ("->"  . ?→)
		      ("=>"  . ?⇒)
		      (">="  . ?≥)
		      ("<="  . ?≤)
		      ("=="  . ?≡)
		      ("!="  . ?≢)
		      (".."  . ?‥)
		      ("..." . ?…))))))

(use-package salt-mode)
(use-package geiser)
(setf telega-chat-input-markups '("org" nil))
;;; theme.el --- Enable and configure the theme

(use-package doom-themes
  :config
  (setq doom-themes-enable-bold t
	doom-themes-enable-italic t)
  (load-theme 'doom-flatwhite t)		; Main color theme
  
  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; Custom neotree theme
  (doom-themes-neotree-config)
  (doom-themes-org-config))
;;; todoist.el --- Configuration for todoist

(use-package todoist
  :commands todoist
  :init
  (setq todoist-token "b95fe3f034fd8cd19fc863edb3785c2cb489db1d")
  :custom
  (todoist-backing-buffer "/home/eric/org/agenda/todo.org"))

(use-package zig-mode)

(add-to-list 'load-path "/home/eric/.opam/default/share/emacs/site-lisp")
(require 'ocp-indent)
(use-package opam)

(use-package treemacs)

(use-package treemacs-projectile
	:after (projectile treemacs))

;; Ada programming
(use-package ada-mode :custom (ada-face-backend 'wisi))
(use-package gpr-mode)
(use-package gpr-yasnippets)

;; Nix
(use-package nix-mode)

(add-to-list 'auto-mode-alist '("\\.html.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html.heex\\'" . heex-ts-mode))

;; Avy is a package for jumping to visible text using a decision tree
(use-package avy
	:config
	(global-set-key (kbd "C-:") 'avy-goto-char-2))

;; Dirvish is an elevated Dired
(use-package dirvish
	:config
	(dirvish-override-dired-mode))

;; Terraform
(use-package terraform-mode)

;; PlantUML in org-mode
(org-babel-do-load-languages
 'org-babel-load-languages
 '((plantuml . t))) 

(setq org-plantuml-jar-path "/usr/share/java/plantuml/plantuml.jar")
(use-package plantuml-mode)

;;  *always* use tree-sitter
(use-package treesit-auto
	:config (global-treesit-auto-mode))

(defun configuration ()
	(interactive)
	(find-file
	 (concat (file-name-as-directory (concat
																		(file-name-as-directory user-emacs-directory)
																		"lib"))
					 "configuration.el")))


;; ## added by OPAM user-setup for emacs / base ## 56ab50dc8996d2bb95e7856a6eddb17b ## you can edit, but keep this line
(require 'opam-user-setup "~/.emacs.d/opam-user-setup.el")
;; ## end of OPAM user-setup addition for emacs / base ## keep this line
